import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  hideLogin: BehaviorSubject<boolean>;
  username: BehaviorSubject<string>;
  partnerId: BehaviorSubject<number>;
  isAdmin: BehaviorSubject<boolean>;
  header: BehaviorSubject<string>;
  constructor() { 
    this.hideLogin = new BehaviorSubject<boolean>(false);
    this.username = new BehaviorSubject<string>('');
    this.partnerId = new BehaviorSubject<number>(0);
    this.isAdmin = new BehaviorSubject<boolean>(false);
    this.header = new BehaviorSubject<string>('');
  }
}
