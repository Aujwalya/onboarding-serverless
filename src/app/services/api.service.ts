import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { APICONST } from '../constants/api.constant';
import { AbstractControl } from '@angular/forms';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }
  
  signInUser(payload: any) {
    return this.http.post(`${APICONST.BASE}${APICONST.SIGNINUSER}`, payload);
  }

  registerUser(payload: any) {
    return this.http.post(`${APICONST.BASE}${APICONST.REGISTERUSER}`, payload);
  }

  addPartner(payload: any) {
    return this.http.post(`${APICONST.BASE}${APICONST.ADDPARTNER}`, payload);
  }

  addMappings(selectedtype: string, payload:any) {
    return this.http.post(`${APICONST.BASE}${APICONST.ADDMAPPINGS}/${selectedtype}`, payload);
  }

  createjoltspec(payload: any) {
    return this.http.post(`${APICONST.JOLTBASE}${APICONST.CREATEJOLT}`, payload);
  }

  addJoltSchemaPartnerEndpoint(type: string, payload: any) {
    return this.http.post(`${APICONST.BASE}${APICONST.ADDJOLTSCHEMA}/${type}`, payload);
  }

  getKeysbyEndpoint(apitype: any, joltType: any) {
    return this.http.get(`${APICONST.BASE}${APICONST.GETKEYSBYENDPOINT}/${apitype}/${joltType}`);
  }

  addPartnerEndpoint(payload: any) {
    return this.http.post(`${APICONST.BASE}${APICONST.ADDPARTNERENDPOINT}`, payload);
  }

  getPartnerEndpointInfo(sourceEndpointId: any, partnerId: any) {
    return this.http.get(`${APICONST.BASE}${APICONST.GETPARTNERENDPOINTINFO}/${sourceEndpointId}/${partnerId}`);
  }

  getPartnerMappings(partnerEndpointId: any,type: any) {
    return this.http.get(`${APICONST.BASE}${APICONST.GETPARTNERMAPPINGS}/${partnerEndpointId}/${type}`);
  }

  // Admin Endpoint APIs
  saveEndpoint(endpointArray: any) {
    // localStorage.setItem("endpointConfig", JSON.stringify([{name:"ep1",request:["x","y","z"], response:["a","b","c"]}]));
    if(localStorage.getItem("endpointConfig")){
      let existingArray = JSON.parse(localStorage.getItem("endpointConfig"));
      let modifiedArray = existingArray.concat(endpointArray);
      localStorage.setItem("endpointConfig", JSON.stringify(modifiedArray));
    } else {
      localStorage.setItem("endpointConfig", JSON.stringify(endpointArray));
    }  
  }

  getEndpointConfig() {
    return JSON.parse(localStorage.getItem("endpointConfig"));
  }

  updateEndpoint(endpointArray: any, index: number) {
    if(localStorage.getItem("endpointConfig")){
      let existingArray = JSON.parse(localStorage.getItem("endpointConfig"));
      existingArray[index] = endpointArray[0];
      localStorage.setItem("endpointConfig", JSON.stringify(existingArray));
    }
  }

  deleteEndpoint(endpointName: string) {
    if(localStorage.getItem("endpointConfig")){
      let modifiedArray = [];
      let existingArray = JSON.parse(localStorage.getItem("endpointConfig"));
      existingArray.forEach(dataarray => {
        if(!(dataarray.endpointname === endpointName)) {
          modifiedArray = modifiedArray.concat(dataarray);
        }
      });
      localStorage.setItem("endpointConfig", JSON.stringify(modifiedArray));
    }
  }

  // Partner Endpoint APIs

  getPartnerEndpointData(id) {
    let pid = "partnerEndpointData" + id;
    return JSON.parse(localStorage.getItem(pid));
  }

  savePartnerData(id, mappingArray) {
    let pid = "partnerEndpointData" + id;
    if(localStorage.getItem(pid)){
      let existingArray = JSON.parse(localStorage.getItem(pid));
      let modifiedArray = []; 
      let modifiedAdded = false;
      existingArray.forEach(arr => {
        if (arr.endpointname === mappingArray[0].endpointname) {
          modifiedArray.push(mappingArray[0]);
          modifiedAdded = true;
        } else {
          modifiedArray.push(arr);
        }
      });
      if (!modifiedAdded) {    
        modifiedArray = existingArray.concat(mappingArray[0]);;
      }
      
      localStorage.setItem(pid, JSON.stringify(modifiedArray));
    } else {
      localStorage.setItem(pid, JSON.stringify(mappingArray));
    } 
  }

  // Admin Profile APIs

  getProfileConfig() {
    return JSON.parse(localStorage.getItem("profileConfig"));
  }

  saveProfileConfig(profileArray) {
    localStorage.setItem("profileConfig", JSON.stringify(profileArray));
  }

  // Partner Profile APIs

  savePartnerProfileData(partnerArray, id) {
    let pid = "profileData" + id;
    localStorage.setItem(pid,  JSON.stringify(partnerArray));
  }

  getPartnerProfileData(id) {
    let pid = "profileData" + id;
    return JSON.parse(localStorage.getItem(pid));
  }

  minLengthArray(min: number) {
    return (c: AbstractControl): { [key: string]: any } => {
    if (c.value.length >= min) return null;
      return { minLengthArray: { valid: false } };
    };
  }
}
