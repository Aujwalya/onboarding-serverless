import { Component } from '@angular/core';
import { StoreService } from './services/store.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  hideLogin : boolean;
  headerString: string;
  constructor(
    private storeService: StoreService
  ) {}
  
  ngOnInit(): void { 
    this.storeService.hideLogin.subscribe(loginbool => {
      this.hideLogin = loginbool;
    });

    this.storeService.header.subscribe(header => {
     this.headerString = header;
    });
    // this.hideLogin = true;
    // this.storeService.partnerId.next(64);
  }

  logout (logoutString: string) : void {
    if (logoutString === 'logout') {   
      this.storeService.hideLogin.next(false);
      this.storeService.isAdmin.next(false);
      this.storeService.partnerId.next(0);
      this.storeService.username.next('');
    }
  }

}
