import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponentComponent } from '../pages/modal-component/modal-component.component';
import { ApiService } from 'src/app/services/api.service';
import { StoreService } from 'src/app/services/store.service';

@Component({
  selector: 'app-partner',
  templateUrl: './partner.component.html',
  styleUrls: ['./partner.component.scss']
})
export class PartnerComponent implements OnInit {

  partnerEpForm: FormGroup;
  endpointData: any;
  activeEndpoint: any;
  currentIndex: number;
  partnerData: any;
  partnerId: number;

  constructor(
    private formBuilder: FormBuilder,
    private api: ApiService,
    private modalService: NgbModal,
    private storeService: StoreService
  ) { }

  ngOnInit(): void {
    this.endpointData = this.api.getEndpointConfig();
    if (this.endpointData.length > 0) {
      this.assignEndpointToForm(0);
    } 
    // else {
    //   console.log('Endpoints not defined');
    // }
  }

  assignEndpointToForm(index) {
    this.storeService.partnerId.subscribe(partnerId => {
      this.partnerId = partnerId;
    });
    this.partnerData = this.api.getPartnerEndpointData(this.partnerId);
    this.activeEndpoint = this.endpointData[index];
    let currentEndpointData;
    if(this.partnerData) {
      this.partnerData.forEach(pd => {
        if( pd.endpointname === this.activeEndpoint.endpointname) {
          currentEndpointData = pd;
        }
      });
    }

    this.partnerEpForm = this.formBuilder.group({
      name: new FormControl(currentEndpointData && currentEndpointData.name? currentEndpointData.name: '', [Validators.required]),
      url: new FormControl(currentEndpointData && currentEndpointData.url? currentEndpointData.url:'', [Validators.required])
    });
    if (this.activeEndpoint.request) {
      this.addControlstoForm(this.activeEndpoint.request, currentEndpointData, 'Request');
    }
    if (this.activeEndpoint.response) {
      this.addControlstoForm(this.activeEndpoint.response, currentEndpointData, 'Response');
    }
    this.currentIndex = 0;
  }

  addControlstoForm(keys, currentEndpointData, type) {
    if(type === 'Request') {
      if(currentEndpointData && currentEndpointData.request){
        currentEndpointData.request.forEach(mapping => {
          this.partnerEpForm.addControl(mapping.key, new FormControl(mapping.value));
        });
      } else{
        keys.forEach(key => {
          this.partnerEpForm.addControl(key, new FormControl(''));
        });
      }   
    } else {
      if(currentEndpointData && currentEndpointData.response){
        currentEndpointData.response.forEach(mapping => {
          this.partnerEpForm.addControl(mapping.key, new FormControl(mapping.value));
        });
      } else{
        keys.forEach(key => {
          this.partnerEpForm.addControl(key, new FormControl(''));
        });
      }
    } 
  }

  get f() { return this.partnerEpForm.controls; }

  saveMappings() {
    let mappingArray = [];
    let reqArray = [];
    let respArray = [];

    this.activeEndpoint.request.forEach(req => {
      reqArray.push({
        key: req,
        value:this.partnerEpForm.value[req]
      });
    });

    this.activeEndpoint.response.forEach(resp => {
      respArray.push({
        key: resp,
        value: this.partnerEpForm.value[resp]
      });
    });

    mappingArray.push({
      endpointname: this.activeEndpoint.endpointname,
      name: this.partnerEpForm.value.name,
      url: this.partnerEpForm.value.url,
      request: reqArray,
      response: respArray
    });
    this.api.savePartnerData(this.partnerId, mappingArray);
    this.openModel('Endpoint Mapping Saved');
  }

  openModel(message: string) {
    const modalRef = this.modalService.open(ModalComponentComponent);
    modalRef.componentInstance.message = message;
  }
}
