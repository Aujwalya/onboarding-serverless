import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PartnerProfileComponent } from './partner-profile/partner-profile.component';
import { PartnerComponent } from './partner.component';

const routes: Routes = [
  { path: '', component: PartnerComponent },
  { path: 'endpoint', component: PartnerComponent },
  { path: 'profile', component: PartnerProfileComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PartnerRoutingModule { }
