import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PartnerRoutingModule } from './partner-routing.module';
import { PartnerComponent } from './partner.component';
import { PartnerProfileComponent } from './partner-profile/partner-profile.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    PartnerComponent,
    PartnerProfileComponent
  ],
  imports: [
    CommonModule,
    PartnerRoutingModule,
    ReactiveFormsModule
  ]
})
export class PartnerModule { }
