import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponentComponent } from 'src/app/pages/modal-component/modal-component.component';
import { ApiService } from 'src/app/services/api.service';
import { StoreService } from 'src/app/services/store.service';

@Component({
  selector: 'app-partner-profile',
  templateUrl: './partner-profile.component.html',
  styleUrls: ['./partner-profile.component.scss']
})
export class PartnerProfileComponent implements OnInit {

  public partnerId: number;
  public fields: any;
  public partnerData: any;
  public profileForm: FormGroup;
  // public fields = [{
  //   label: "Partner Name",
  //   type: "text"
  // }]

  constructor (
    private api: ApiService,
    private storeService: StoreService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this.storeService.partnerId.subscribe(partnerId => {
      this.partnerId = partnerId;
    });
    this.fields = this.api.getProfileConfig();
    // this.fields = Array.of(this.fields); 
    this.partnerData = this.api.getPartnerProfileData(this.partnerId);
    this.populateData();
  }

  populateData(): void {
    this.profileForm = this.formBuilder.group({});
    if(this.partnerData && this.fields) {
      this.fields.forEach(keyValuePair => {
        this.profileForm.addControl(keyValuePair.label, new FormControl(this.partnerData[keyValuePair.label]));
      });
    } else if (this.fields){
      this.fields.forEach(keyValuePair => {
        this.profileForm.addControl(keyValuePair.label, new FormControl(''));
      });
    }
  }

  savePartnerData(): void {
    let partnerDataArray = {};
    this.fields.forEach(keyValuePair => {
      partnerDataArray[keyValuePair.label] = this.profileForm.value[keyValuePair.label];
    });

    this.api.savePartnerProfileData(partnerDataArray, this.partnerId);
    this.openModel("Partner Profile Data Saved.")
  }

  openModel(message: string) {
    const modalRef = this.modalService.open(ModalComponentComponent);
    modalRef.componentInstance.message = message;
  }

}
