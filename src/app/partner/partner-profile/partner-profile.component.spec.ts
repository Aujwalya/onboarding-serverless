import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { MockDataService } from 'src/test-mocks/mock-api.service';

import { PartnerProfileComponent } from './partner-profile.component';

describe('PartnerProfileComponent', () => {
  let component: PartnerProfileComponent;
  let fixture: ComponentFixture<PartnerProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartnerProfileComponent ],
      imports: [ReactiveFormsModule],
      providers: [
        { provide: ApiService, useClass: MockDataService }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });

  it('should call savePartnerData', () => {
    const spy = spyOn(component, 'savePartnerData');
    component.savePartnerData();
    expect(spy).toHaveBeenCalled();
  });
});
