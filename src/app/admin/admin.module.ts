import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { AppModule } from '../app.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AdminProfileEditorComponent } from './admin-profile-editor/admin-profile-editor.component';


@NgModule({
  declarations: [
    AdminComponent,
    AdminProfileEditorComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    ReactiveFormsModule
  ]
})
export class AdminModule { }
