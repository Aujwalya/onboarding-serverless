import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponentComponent } from 'src/app/pages/modal-component/modal-component.component';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-admin-profile-editor',
  templateUrl: './admin-profile-editor.component.html',
  styleUrls: ['./admin-profile-editor.component.scss'],
})
export class AdminProfileEditorComponent implements OnInit {
  profileForm: FormGroup;
  profileData: any;
  showErrorMsg: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private api: ApiService,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.showErrorMsg = false;
    this.profileData = this.api.getProfileConfig();
    if (this.profileData?.length > 0) {
      this.assignProfileToForm();
    } else {
      this.addNew();
    }
  }

  assignProfileToForm() {
    let mappedProfileArray = [];
    this.profileData.forEach((element) => {
      mappedProfileArray.push(element.label);
    });
    this.profileForm = this.formBuilder.group({
      profileItem: this.formBuilder.array(mappedProfileArray),
    });
  }

  addNew() {
    this.profileForm = this.formBuilder.group({
      profileItem: this.formBuilder.array([''], this.api.minLengthArray(1)),
    });
  }

  get profileItem() {
    return this.profileForm.get('profileItem') as FormArray;
  }

  addItem() {
    this.showErrorMsg = false;
    this.profileItem.push(this.formBuilder.control(''));
  }

  deleteItem(index: any) {
    if (index === 0) {
      this.showErrorMsg = true;
      /* document.getElementById('errorMsg').innerText =
        'Minimum input field required'; */
      return;
    } else {
      this.profileItem.removeAt(index);
    }
  }

  onSubmit() {
    let profileFormArray = [];
    this.profileForm.value.profileItem.forEach((element) => {
      profileFormArray.push({
        label: element,
        type: 'text',
      });
    });

    this.api.saveProfileConfig(profileFormArray);
    this.openModel('Profile Created/Updated');
    this.profileData = this.api.getProfileConfig();
    this.assignProfileToForm();
  }

  openModel(message: string) {
    const modalRef = this.modalService.open(ModalComponentComponent);
    modalRef.componentInstance.message = message;
  }
}
