import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { MockDataService } from 'src/test-mocks/mock-api.service';

import { AdminProfileEditorComponent } from './admin-profile-editor.component';

describe('AdminProfileEditorComponent', () => {
  let component: AdminProfileEditorComponent;
  let fixture: ComponentFixture<AdminProfileEditorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminProfileEditorComponent ],
      imports: [ReactiveFormsModule],
      providers: [
        { provide: ApiService, useClass: MockDataService }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminProfileEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call onSubmit', () => {
    const spy = spyOn(component, 'onSubmit');
    component.onSubmit();
    expect(spy).toHaveBeenCalled();
  });

  it('should call addItem', () => {
    const spy = spyOn(component, 'addItem');
    component.addItem();
    expect(spy).toHaveBeenCalled();
  });

  it('should call deleteItem', () => {
    const spy = spyOn(component, 'deleteItem');
    component.deleteItem(1);
    expect(spy).toHaveBeenCalled();
  });
});
