import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { ApiService } from '../services/api.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponentComponent } from '../pages/modal-component/modal-component.component';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})

export class AdminComponent implements OnInit {
  epForm: FormGroup;
  endpointData: any;
  activeEndpoint: string;
  currentIndex: number;

  constructor(
    private formBuilder: FormBuilder, 
    private api: ApiService,
    private modalService: NgbModal) {}

  ngOnInit(): void {
    this.endpointData = this.api.getEndpointConfig(); 
    if(this.endpointData?.length > 0) {
      this.assignFirstEndpointtoForm();
    } else {
      this.addNew();
    }
  }

  assignFirstEndpointtoForm() {
    this.epForm = this.formBuilder.group({
      endpointname: [this.endpointData[0].endpointname,Validators.required],
      description: [this.endpointData[0].description, Validators.required],
      radioType: ['request'],
      requestItem: this.formBuilder.array(this.endpointData[0].response),
      responseItem: this.formBuilder.array(this.endpointData[0].request)
    });
    this.activeEndpoint = this.epForm.get('endpointname').value;
    this.currentIndex = 0;
  }

  addNew() {
    this.epForm = this.formBuilder.group({
      endpointname: ['', Validators.required],
      description: ['', Validators.required],
      radioType: ['request'],
      requestItem: this.formBuilder.array([]),
      responseItem: this.formBuilder.array([]),
    });
    this.activeEndpoint = '';
    this.currentIndex = 0;
  }

  getEndpoint(index: number) {
    this.epForm = this.formBuilder.group({
      endpointname: [this.endpointData[index].endpointname],
      description: [this.endpointData[index].description],
      radioType: ['request'],
      requestItem: this.formBuilder.array(this.endpointData[index].response),
      responseItem: this.formBuilder.array(this.endpointData[index].request)
    });
    this.activeEndpoint = this.epForm.get('endpointname').value;
    this.currentIndex = index;
  }

  get requestItem() {
    return this.epForm.get('requestItem') as FormArray;
  }
  get responseItem() {
    return this.epForm.get('responseItem') as FormArray;
  }

  addItem() {
    let radioSelected = this.epForm.value.radioType;

    if (radioSelected === 'request') {
      this.requestItem.push(this.formBuilder.control(''));
    } else {
      this.responseItem.push(this.formBuilder.control(''));
    }
  }

  deleteRequest(index: any) {
    this.requestItem.removeAt(index);
  }

  deleteResponse(index: any) {
    this.responseItem.removeAt(index);
  }

  onSubmit() {
    let epFormArray = [];
    epFormArray.push({
      endpointname: this.epForm.value.endpointname,
      description: this.epForm.value.description,
      request: this.epForm.value.requestItem,
      response: this.epForm.value.responseItem,
    });
    if(this.activeEndpoint) {
      this.updateEndpoint(this.currentIndex, epFormArray);
    } else {
      this.createEndpoint(epFormArray);
    }
    
  }

  deleteEndpoint(index: number) {
    if(confirm("Are you sure to delete "+ index)) {
      this.api.deleteEndpoint(this.endpointData[index].endpointname);    
      this.openModel('Endpoint Deleted');
      this.endpointData = this.api.getEndpointConfig();
      this.assignFirstEndpointtoForm();
    };
  }

  updateEndpoint(index: number, epFormArray) {
    
    this.api.updateEndpoint(epFormArray, index);
    this.openModel('Endpoint Updated');
    this.endpointData = this.api.getEndpointConfig();
    this.assignFirstEndpointtoForm();
  }

  createEndpoint(epFormArray) {
    this.api.saveEndpoint(epFormArray);
    this.openModel('Endpoint Created');
    this.endpointData = this.api.getEndpointConfig();
    this.assignFirstEndpointtoForm();
  }

  openModel(message: string) {
    const modalRef = this.modalService.open(ModalComponentComponent);
    modalRef.componentInstance.message = message;
  }

}

