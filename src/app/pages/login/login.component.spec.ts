import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ApiService } from 'src/app/services/api.service';
import { MockDataService } from 'src/test-mocks/mock-api.service';

import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [ReactiveFormsModule, RouterTestingModule],
      providers: [
        { provide: ApiService, useClass: MockDataService }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call onLogin', () => {
    const spy = spyOn(component, 'onLogin');
    component.onLogin();
    expect(spy).toHaveBeenCalled();
  });

  it('should call onSignUp', () => {
    const spy = spyOn(component, 'onSignUp');
    const event = new MouseEvent('click'); 
    component.onSignUp(event);
    expect(spy).toHaveBeenCalled();
  });
});
