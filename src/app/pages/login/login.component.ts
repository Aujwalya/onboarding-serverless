import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { ApiService } from '../../services/api.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponentComponent } from '../modal-component/modal-component.component';
import { StoreService } from '../../services/store.service';
import { Router } from '@angular/router';
import { HEADERCONST } from '../../constants/header.constant'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  signupForm: FormGroup;
  tabActive = 'logintab';
  loginApiMessage: string;
  signUpApiMessage: string;
  signUpApiError: string;
  partnerId: number;
  hidePartnerLogin = false;
  hideAdminLogin = true;
  showConfigMapping: boolean;
  showProfileInformation: boolean;
  useremail: string;
  username: string;
  isNew: boolean = false;

  constructor(
    private formBuilder: FormBuilder, 
    private api: ApiService,
    private cd: ChangeDetectorRef, 
    private spinnerService: NgxSpinnerService, 
    private modalService: NgbModal,
    private storeService: StoreService,
    private router: Router) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
    this.signupForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  get l() { return this.loginForm.controls; }
  get s() { return this.signupForm.controls; }

  toggleLoginSignUp(e) {
    this.tabActive = e.target.id;
    this.signupForm.reset();
    this.loginForm.reset();
    this.loginApiMessage = '';
    this.signUpApiMessage = '';
    this.signUpApiError = '';
  }

  onLogin() : void{
    this.spinnerService.show();
    this.loginApiMessage = '';
    let payload = {
      email: this.loginForm.value.email,
      password: this.loginForm.value.password
    }

    this.api.signInUser(payload).subscribe((resp: any) => {
        let email = resp.data[0].user_email;
        this.useremail = email;
        this.username = email.split('@')[0].charAt(0).toUpperCase() + email.split('@')[0].slice(1);
        let partnerId = resp.data[0].partner_id;       
        this.spinnerService.hide();
        this.partnerId = partnerId;
        this.storeService.partnerId.next(this.partnerId);
        if (partnerId) {
          this.showConfigMapping = true;
          // this.hideLogin = true;
          this.storeService.hideLogin.next(true);
          if (this.hidePartnerLogin) {
            this.storeService.isAdmin.next(true);
            this.storeService.header.next(HEADERCONST.ADMINENDPOINT);
            this.router.navigate(['/admin']);
          } else {
            this.storeService.header.next(HEADERCONST.PARTNERENDPOINT);
            this.router.navigate(['/partner']);
          }
        } else {
          this.showProfileInformation = true;
          // this.hideLogin = true;
          this.storeService.hideLogin.next(true);
          if (this.hidePartnerLogin) {
            this.storeService.isAdmin.next(true);
            this.storeService.header.next(HEADERCONST.ADMINPROFILE);
            this.router.navigate(['/admin/profile']);
          } else {
            this.storeService.header.next(HEADERCONST.PARTNERPROFILE);
            this.router.navigate(['/partner/profile']);
          }     
        }
      
    }, (e) => {
      this.spinnerService.hide();
      this.loginApiMessage = e.error.message;
    }
    );
  }

  onSignUp (evt) : void {
    this.spinnerService.show();
    evt.preventDefault();
    this.signUpApiMessage = '';
    this.signUpApiError = '';
    var payload = {
      email: this.signupForm.value.email,
      password: this.signupForm.value.password,
      createdOn: new Date(Date.now()).toISOString()
    }

    this.api.registerUser(payload).subscribe((resp: any) => {
        this.spinnerService.hide();     
        this.signUpApiMessage = resp.message;
        this.signupForm.reset();
    }, (e) => {
      this.spinnerService.hide();
      this.signUpApiError = e.error.message;
      this.signupForm.reset();
    });
  }

  adminLogin() {
    this.hideAdminLogin = false;
    this.hidePartnerLogin = true;  
    this.signupForm.reset();
    this.loginForm.reset();
  }

  partnerLogin() {
    this.hideAdminLogin = true
    this.hidePartnerLogin = false; 
    this.signupForm.reset();
    this.loginForm.reset(); 
  }

  open (message: string) : void{
    const modalRef = this.modalService.open(ModalComponentComponent);
    modalRef.componentInstance.message = message;
  }


}

