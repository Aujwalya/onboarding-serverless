import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { NavbarComponent } from './navbar.component';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavbarComponent ],
      imports: [RouterTestingModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call navigatetoEndpoint', () => {
    const spy = spyOn(component, 'navigatetoEndpoint');
    component.navigatetoEndpoint();
    expect(spy).toHaveBeenCalled();
  });

  it('should call navigatetoProfile', () => {
    const spy = spyOn(component, 'navigatetoProfile');
    component.navigatetoProfile();
    expect(spy).toHaveBeenCalled();
  });

  it('should call supportClicked', () => {
    const spy = spyOn(component, 'supportClicked');
    component.supportClicked();
    expect(spy).toHaveBeenCalled();
  });

  it('should call logoutClicked', () => {
    const spy = spyOn(component, 'logoutClicked');
    component.logoutClicked();
    expect(spy).toHaveBeenCalled();
  });
});
