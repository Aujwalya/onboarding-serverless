import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { StoreService } from '../../services/store.service';
import { HEADERCONST } from '../../constants/header.constant';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  @Input() username: string;
  @Input() partnerId: string;
  @Output() logout = new EventEmitter<string>();
  isAdmin: boolean;

  showSupportDiv: boolean = false;

  constructor(private storeService: StoreService, private router: Router) {}

  ngOnInit(): void {
    this.storeService.isAdmin.subscribe((isAdminBool) => {
      this.isAdmin = isAdminBool;
    });
  }

  navigatetoEndpoint(): void {
    if (this.isAdmin) {
      this.storeService.header.next(HEADERCONST.ADMINENDPOINT);
      this.router.navigate(['/admin']);
    } else {
      this.storeService.header.next(HEADERCONST.PARTNERENDPOINT);
      this.router.navigate(['/partner']);
    }
    this.showSupportDiv = false;
  }

  navigatetoProfile(): void {
    if (this.isAdmin) {
      this.storeService.header.next(HEADERCONST.ADMINPROFILE);
      this.router.navigate(['/admin/profile']);
    } else {
      this.storeService.header.next(HEADERCONST.PARTNERPROFILE);
      this.router.navigate(['/partner/profile']);
    }
    this.showSupportDiv = false;
  }

  supportClicked():void {
    this.showSupportDiv = this.showSupportDiv ? false : true;
  }

  logoutClicked(): void {
    this.logout.emit('logout');
  }
}
