export const APICONST = {
    BASE: 'https://hfz9u5otqi.execute-api.us-east-1.amazonaws.com',
    JOLTBASE: 'https://c3pivg96tc.execute-api.us-east-1.amazonaws.com',
    ADDPARTNER: '/dev/addPartner',
    SIGNINUSER: '/dev/signInUser',
    REGISTERUSER: '/dev/registerUser',
    ADDMAPPINGS: '/dev/addMappings',
    CREATEJOLT: '/dev/api/v1/mapper/createjoltspec',
    ADDJOLTSCHEMA: '/dev/addJoltSchemaForPartnerEndpoint',
    GETKEYSBYENDPOINT: '/dev/getKeysbyEndpoint',
    ADDPARTNERENDPOINT: '/dev/addPartnerEndpoint',
    GETPARTNERENDPOINTINFO: '/dev/getPartnerEndpointInfo',
    GETPARTNERMAPPINGS: '/dev/getPartnerMappings'
};