export const HEADERCONST = {
    ADMINENDPOINT: 'API Endpoint Editor',
    PARTNERENDPOINT: 'Setup your API Endpoints',
    PARTNERPROFILE: 'Fill out your Partner Profile',
    ADMINPROFILE: 'Profile Editor'
};