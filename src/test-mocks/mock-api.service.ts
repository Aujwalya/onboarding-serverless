import { AbstractControl } from '@angular/forms';
import { Observable, of } from 'rxjs';

export class MockDataService {

    signInUser() {
        return of({
            data: [{
                message: "Successfully logged In",
                user_email: "mb@abc.com",
                partner_id: 67
            }]
        });
    }

    registerUser() {
        return of({
            data: [{
                message: "User is created"
            }]
        });
    }

    addPartner() {
        return of({
            data: [{
                message: "Partner created",
                id: 68
            }]
        });
    }

    addMappings() {
        return of({
            message: "Successfully updated"
        });
    }

    createjoltspec() {
        return of({
            data: [{
                operation: "shift",
                spec: {
                    menu:
                        [{
                            id: "menu[#2].itemId",
                            name: "menu[#2].itemName",
                            rating: "menu[#2].itemRating"
                        }]
                }
            }]
        });
    }

    addJoltSchemaPartnerEndpoint() {
        return of({
            message: "Jolt Schema for partner endpoint has been added succesfully."
        });
    }

    getKeysbyEndpoint() {
        return of({
            data: [{
                id: 1,
                key_name: "menu[0].itemId"
            }, {
                id: 2,
                key_name: "menu[0].itemName"
            }, {
                id: 3,
                key_name: "menu[0].itemRating"
            }
            ]
        });
    }

    addPartnerEndpoint() {
        return of({
            data: [{
                display_name: "Get Menu Partner EndPoint Edited",
                partnerendpointid: 295,
                url: "https://c3pivg96tc.execute-api.us-east-1.amazonaws.com/dev/api/v1/mockmenu/1"
            }]
        });
    }

    getPartnerEndpointInfo() {
        return of({
            data: [{
                display_name: "Get Menu Partner EndPoint",
                partnerendpointid: 295,
                url: "https://c3pivg96tc.execute-api.us-east-1.amazonaws.com/dev/api/v1/mockmenu/1"
            }]
        });
    }

    getPartnerMappings() {
        return of({
            data: [{
                key_name: "menu[0].itemId",
                mapping_value: "menu[0].id",
                mappingid: 550
            }, {
                key_name: "menu[0].itemName",
                mapping_value: "menu[0].name",
                mappingid: 550
            }, {
                key_name: "menu[0].itemRating",
                mapping_value: "menu[0].rating",
                mappingid: 550
            }]
        });
    }

    // Admin Endpoint APIs
    saveEndpoint(endpointArray: any) {
        // localStorage.setItem("endpointConfig", JSON.stringify([{name:"ep1",request:["x","y","z"], response:["a","b","c"]}]));
        if (localStorage.getItem("endpointConfig")) {
            let existingArray = JSON.parse(localStorage.getItem("endpointConfig"));
            let modifiedArray = existingArray.concat(endpointArray);
            localStorage.setItem("endpointConfig", JSON.stringify(modifiedArray));
        } else {
            localStorage.setItem("endpointConfig", JSON.stringify(endpointArray));
        }
    }

    getEndpointConfig() {
        return of({
            data: [{
                name: "ep1",
                request: ["x","y","z"],
                response: ["a","b","c"]
            }]
        });
    }

    // Partner Endpoint APIs

    getPartnerEndpointData(id) {
        let pid = "partnerEndpointData" + id;
        return JSON.parse(localStorage.getItem(pid));
    }

    // Admin Profile APIs

    getProfileConfig() {
        return of([{
                label: "address",
                type: "text"
            }]);
    }

    // Partner Profile APIs

    getPartnerProfileData(id) {
        let pid = "profileData" + id;
        return JSON.parse(localStorage.getItem(pid));
    }

    minLengthArray(min: number) {
        return (c: AbstractControl): { [key: string]: any } => {
        if (c.value.length >= min) return null;
          return { minLengthArray: { valid: false } };
        };
    }
}
